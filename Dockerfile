FROM alpine:3.11

# ensure www-data user exists
RUN set -eux; \
    addgroup -g 82 -S www-data; \
    adduser -u 82 -D -S -G www-data www-data

RUN set -eux; \
    apk --update --no-cache add \
        ca-certificates \
        curl \
        openssl \
        php7-bcmath \
        php7-bz2 \
        php7-calendar \
        php7-cli \
        php7-common \
        php7-curl \
        php7-dom \
        php7-embed \
        php7-fileinfo \
        php7-gd \
        php7-gmp \
        php7-iconv \
        php7-intl \
        php7-json \
        php7-ldap \
        php7-mbstring \
        php7-mysqlnd \
        php7-opcache \
        php7-openssl \
        php7-pcntl \
        php7-pdo \
        php7-pdo_mysql \
        php7-pdo_sqlite \
        php7-redis \
        php7-session \
        php7-simplexml \
        php7-sodium \
        php7-sockets \
        php7-sqlite3 \
        php7-tokenizer \
        php7-xmlwriter \
        php7-xdebug \
        php7-xml \
        php7-zip \
        su-exec \
    ; \
    apk --update --no-cache add --repository=http://dl-cdn.alpinelinux.org/alpine/edge/community \
        unit \
        unit-php7 \
    ;

STOPSIGNAL SIGTERM

COPY docker-entrypoint.sh /usr/local/bin/

# forward log to docker log collector
RUN set -eux; \
    ln -sf /dev/stdout /var/log/unit.log ; \
    mkdir /docker-entrypoint.d/ ; \
    chmod a+x /usr/local/bin/docker-entrypoint.sh

ENTRYPOINT ["/usr/local/bin/docker-entrypoint.sh"]

CMD ["unitd", "--no-daemon", "--control", "unix:/var/run/control.unit.sock", "--tmp", "/tmp"]
